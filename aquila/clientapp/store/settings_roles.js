export const state = () => ({
    isLoading: false
})

const initState = state()

// mutations defined as object
export const mutations = {
  resetState(state) {
    Object.assign(state, initState)
  },
  setState(state, params) {
    const keys = Object.keys(params)
    keys.forEach(key => (state[key] = params[key]))
  },
}

export const actions = {
	// Reset user states
	resetState({ commit }) {
		commit('resetState')
	},
	getAllRoles(context, { token }) {
		// Setting Axios Options
		const axiosOptions = {
				validateStatus: status => {
						const result = status >= 200 && status <= 505
						return result
				},
				headers: {
						'Content-Type': 'application/json-patch+json; charset=utf-8',
						Accept: 'application/json; charset=utf-8',
						Authorization: `Bearer ${token}`
				}
		}

		// Return Axios Promise
		return this.$axios.$get('/5ea7d3aa2f00002534c4f01f', axiosOptions)
	},
	getAllPermissions(context, { token }) {
		// Setting Axios Options
		const axiosOptions = {
				validateStatus: status => {
						const result = status >= 200 && status <= 505
						return result
				},
				headers: {
						'Content-Type': 'application/json-patch+json; charset=utf-8',
						Accept: 'application/json; charset=utf-8',
						Authorization: `Bearer ${token}`
				}
		}

		// Return Axios Promise
		return this.$axios.$get('/5ea7d3c92f00004328c4f024', axiosOptions)
	},
	createRole(context, { token, name, displayName, description, grantedPermissions }) {
		// Setting Axios Options
		const axiosOptions = {
				validateStatus: status => {
						const result = status >= 200 && status <= 505
						return result
				},
				headers: {
						'Content-Type': 'application/json-patch+json; charset=utf-8',
						Accept: 'application/json; charset=utf-8',
						Authorization: `Bearer ${token}`
				}
		}

		// Construct the body parameter
		const body = {
			name,
			displayName,
			description,
			grantedPermissions
		}

		// Return Axios Promise
		return this.$axios.$post('/5ea7d3e12f00003f33c4f025', body, axiosOptions)
	},
	deleteRole(context, { token, id }) {
		// Setting Axios Options
		const axiosOptions = {
				validateStatus: status => {
						const result = status >= 200 && status <= 505
						return result
				},
				headers: {
						'Content-Type': 'application/json-patch+json; charset=utf-8',
						Accept: 'application/json; charset=utf-8',
						Authorization: `Bearer ${token}`
				}
		}

		// Return Axios Promise
		return this.$axios.$delete(`/5ea7d3f42f00002534c4f027`, axiosOptions)
	},
	getDetailRole(context, { token, id }) {
		// Setting Axios Options
		const axiosOptions = {
				validateStatus: status => {
						const result = status >= 200 && status <= 505
						return result
				},
				headers: {
						'Content-Type': 'application/json-patch+json; charset=utf-8',
						Accept: 'application/json; charset=utf-8',
						Authorization: `Bearer ${token}`
				}
		}

		// Return Axios Promise
		return this.$axios.$get(`/5ea7d40a2f00002534c4f029`, axiosOptions)
	},
}

