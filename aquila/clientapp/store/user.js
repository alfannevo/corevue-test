export const state = () => ({
    isLogin: false,
    userId: null,
    accessToken: null,
    refreshToken: null,
    expired: 0,
    username: null,
    email: null
})

const initState = state()

// mutations defined as object
export const mutations = {
  resetState(state) {
    Object.assign(state, initState)
  },
  setState(state, params) {
    const keys = Object.keys(params)
    keys.forEach(key => (state[key] = params[key]))
  },
  setLoginInfo(state, param) {
    const { isLogin, accessToken, refreshToken, expired, username, email } = param

    state.isLogin = isLogin
    state.accessToken = accessToken
    state.refreshToken = refreshToken
		state.expired = expired
    state.username = username !== undefined ? username : null
    state.email = email !== undefined ? email : null
  }
}

export const actions = {
	// Reset user states
	resetState({ commit }) {
		commit('resetState')
	},
	setLoginInfo({ commit }, param) {
		commit('setLoginInfo', param)
	},
	postLogin(context, { username, password }) {
		// Setting Axios Options
		const axiosOptions = {
				validateStatus: status => {
						const result = status >= 200 && status <= 505
						return result
				},
				headers: {
						'Content-Type': 'application/json-patch+json; charset=utf-8',
						Accept: 'application/json; charset=utf-8'
				}
		}

		// Construct the body parameter
		const body = {
				userNameOrEmailAddress: username,
				password,
				rememberClient: true
		}

		// Return Axios Promise
		return this.$axios.$post('/5ea7d3622f00003f33c4f01a', body, axiosOptions)
	},
	async setCookies({ state }) {
		const expireRefresh = parseInt(process.env.EXPIRE_REFRESH) * 1000
		// calculate expires
		const expDate = new Date()
		expDate.setTime(expDate.getTime() + (state.expiredAccessToken - 10) * 1000)
		const expDate2 = new Date()
		expDate2.setTime(expDate2.getTime() + expireRefresh)

		const secure = process.env.NODE_ENV !== 'development'
		const options = {
				path: '/',
				expires: expDate,
				secure,
				sameSite: true
		}

		const options2 = {
				path: '/',
				expires: expDate2,
				secure,
				sameSite: true
		}

		const cookieList = [
				{ name: 'aq_isLogin', value: true, opts: options2 },
				{ name: 'aq_accKey', value: state.accessToken, opts: options },
				{ name: 'aq_refKey', value: state.encryptedAccessToken, opts: options2 },
				{ name: 'aq_userID', value: state.userId, opts: options }
		]

		this.$cookies.setAll(cookieList)
	},
	// actions for user logout
	async doLogout({ commit, dispatch }) {
		// reset user states
		commit('resetState')
		// remove cookies
		await this.$cookies.removeAll()
		// clear localStorage
		// await localStorage.clear()
	},
}

