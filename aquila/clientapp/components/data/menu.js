const Menu = [
  {header: 'Feature'},
  {
    title: 'Home',
    group: 'apps',
    icon: 'dashboard',
    name: 'Home',
    href: '/home'
  },
  {header: 'Management'},
  {
    title: 'Settings',
    group: 'settings',
    component: 'components',
    icon: 'tune',
    items: [
      {name: 'users', title: 'Users', href: '/settings/users'},
      {name: 'roles', title: 'Roles', href: '/settings/roles'}
    ]
  },
  {divider: true},
  {header: 'My Data'},
  {
    title: 'My Profile',
    group: 'apps',
    icon: 'account_circle',
    name: 'My Profile',
    href: '/my_profile'
  },
  {
    title: 'Logout',
    group: 'apps',
    icon: 'open_in_new',
    name: 'Logout',
    href: '/logout'
  },
];
// reorder menu
Menu.forEach((item) => {
  if (item.items) {
    item.items.sort((x, y) => {
      let textA = x.title.toUpperCase();
      let textB = y.title.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
  }
});

export default Menu;
