import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify, {
    options: {
        customProperties: true
    },
    theme: {
        // color: https://nuxt-material-admin.netlify.app/general/colors
        redDarken3: '#C62828'
    },
    minifyTheme(css) {
        return process.env.NODE_ENV === 'production'
            ? css.replace(/[\s|\r\n|\r|\n]/g, '')
            : css
    }
})
