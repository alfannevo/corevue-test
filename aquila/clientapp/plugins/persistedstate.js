import createPersistedState from 'vuex-persistedstate'

export default ({ store, req, isDev }) => {
  createPersistedState({
    key: 'aquila',
    paths: [
      'user.username',
      'user.email'
    ]
  })(store)
}