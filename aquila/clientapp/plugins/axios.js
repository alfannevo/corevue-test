export default function({ $axios, store, redirect, route }) {
    $axios.onRequest(config => {
      // console.log('Request ' + config.method + ' to: ', config.url)
      if (config.method === 'post') {
        // console.log('set global post content type')
        $axios.setHeader('Content-Type', 'application/vnd.api+json', ['post'])
      }
      $axios.setHeader('Accept', 'application/vnd.api+json')
    })
  
    $axios.onRequestError(error => {
      console.log('onRequestError to: ', error.response.data)
    })
  
    $axios.onResponseError(error => {
      const code = parseInt(error.response && error.response.status)
      // 503: Maintenance Mode
      if (code === 503) {
        // Redir to maintenance page
        redirect(`/maintenance?prev=${route.fullPath}`)
      }
    })
  
    $axios.interceptors.response.use(
      response => response,
      error => {
        if (typeof error.response === 'undefined') {
            // General Error
        }
        return Promise.reject(error)
      }
    )
  }