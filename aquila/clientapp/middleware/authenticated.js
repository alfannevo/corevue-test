export default async ({ store, redirect, app, route, router, env }) => {
    const isLogin = app.$cookies.get('aq_isLogin') === 'true'
    const accessKey = app.$cookies.get('aq_accKey') || null
    const refKey = app.$cookies.get('aq_refKey') || null
    const userID = app.$cookies.get('aq_userID') || null

    if (isLogin) {
        store.commit('user/setState', {
            isLogin,
            accessToken: accessKey,
            encryptedAccessToken: refKey,
            userId: userID
        })
    } else {
        return redirect('/login')
    }
}